const assert = require('assert');

describe('AppDirect Market Place Page', () => {
    beforeEach(() => {
        browser.maximizeWindow();
        browser.url('https://marketplace.appdirect.com/');
    });

    after(() => {
        browser.deleteSession();
    });
    

    it(('should open the main marketplace website'), () => {
        const title = $('h2.header--item').getText();
        assert.strictEqual(title, "Featured Applications");
    });

    it(('should search for all applications in the marketplace'), () => {
        const searchBtn = $('.R3aVU4wxg3hp-iBiRop5Y.search_field');
        searchBtn.click();
        const results = $('.header--item.js-listing-content-header-count').getText();
        // console.log(results);
        assert.strictEqual(results, 'Showing 925 Applications');
    });

    xit(('should search for applications in the marketplace'), () => {
        //TODO
        //Messy HTML tag searching. Still need to work on finding the appropriate field tag
    });

    it(('should filter free applications in the marketplace'), () => {
        const searchBtn = $('.R3aVU4wxg3hp-iBiRop5Y.search_field');
        searchBtn.click();
        const freeAttribute = $('.adb-selector:nth-child(3)');
        freeAttribute.click();
        const freePrice = $('.price--value').getText();
        assert.strictEqual(freePrice, "Free");
    });

    it(('should sort applications in the marketplace by popularity'), () => {
        const searchBtn = $('.R3aVU4wxg3hp-iBiRop5Y.search_field');
        searchBtn.click();
        $('.js-dropdown-select.js-grid-filters').selectByVisibleText("Popularity");
        assert(browser.getUrl(), "https://marketplace.appdirect.com/en-US/listing?order=POPULARITY")
    });

    it(('should open application page'), () => {
        $('[href="/en-US/apps/577/g-suite"]').click();
        assert.strictEqual(browser.getTitle(), "G Suite by Google | AppDirect");
    });

});