const assert = require('assert');

describe('AppDirect Main Page', () => {

    beforeEach(() => {
        browser.maximizeWindow()
        browser.url('https://appdirect.com');
    });

    after(() => {
        browser.deleteSession();
    });
  
    it('should have the right title', () => {
        const title = browser.getTitle();
        console.log(title);
        assert.strictEqual(title, 'AppDirect powers the digital economy - AppDirect');
    });
    
    it('should navigate to login page', ()=>{
        // const login = $('.c-nav__link.hide-is-searching.c-nav__link--login').getText();
        // console.log(login);
        $('.c-nav__link.hide-is-searching.c-nav__link--login').click();
        browser.switchWindow('marketplace.appdirect.com')
        const title = browser.getTitle();
        console.log(title);
        assert.strictEqual(title, 'Log In | AppDirect');
        browser.pause(5000);
        // $('.c-button').click();
        // browser.pause(50000)
    });

    it('should navigate to demo page', ()=>{
        const demo = $('.c-button').getText();
        console.log(demo);
        $('.c-button').click();
        $('.c-form__header').waitForExist(5000);
        const demoPage = browser.getTitle();
        assert.strictEqual(demoPage,"Request Demo - AppDirect");
    });

    it('should search from the main page', ()=>{
        const searchBtn = $('.c-nav__link.c-nav__search-btn');
        searchBtn.click();
        const searchField = $('.js-search-input.c-nav__search-input');
        searchField.waitForExist(500);
        searchField.addValue("google");
        browser.keys("\uE007"); 
        const results = $('h1=Search Results');
        results.waitForExist(5000);
        console.log(results.getText());
        assert.strictEqual(results.getText(),"Search Results");
    });
});