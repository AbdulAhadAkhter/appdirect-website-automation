# AppDirect Website Automation

Clone directory and run `npm install` to download dependencies and modules.

Run tests with command `./node_modules/.bin/wdio wdio.conf.js`

You can add the `--spec *.js` above to run a specific test suite rather than running the entire suite e.g. `./node_modules/.bin/wdio wdio.conf.js --spec mainPageTest.js` will only run the main page test suite

Run `allure generate --clean && allure open` to view a nicer looking report than the spec and dot reporters.

Alternatively you can view the report generated [here](https://sleepy-ritchie-dbb67e.netlify.com/). Hosted on Netlify.

